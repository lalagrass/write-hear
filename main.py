#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
import os
import argparse
import base64
import json
import logging

# add 'lib'
from google.appengine.ext import vendor
vendor.add(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lib'))

from googleapiclient import discovery
import httplib2
from oauth2client.client import GoogleCredentials
# [END import_libraries]


# [START authenticating]
DISCOVERY_URL = ('https://{api}.googleapis.com/$discovery/rest?'
                 'version={apiVersion}')
				 
jinja_environment = jinja2.Environment(
		loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
		extensions=['jinja2.ext.autoescape'],
		autoescape=True)

GOOGLE_APPLICATION_CREDENTIALS = jinja_environment.get_template('Write-Hear-aa706d3bb8cf.json')

class MainHandler(webapp2.RequestHandler):
	def get(self):
		target_page = 'index.html'
		template = jinja_environment.get_template(target_page)
		self.response.write(template.render())
	def post(self):
		audio = self.request.get("audioFile")
		target_page = 'index.html'
		template = jinja_environment.get_template(target_page)
		resu = process_audio(audio)
		#logging.info('This is an info message' + res)
		reses = resu['responses']
		strs = ''
		for res in reses:
			results = res['results']
			strs += results[0]['alternatives'][0]['transcript'] + '<br>'
		self.response.write(template.render(
			audioFile = 'processing...<br><br>' + strs,
		))

# Application default credentials provided by env variable
# GOOGLE_APPLICATION_CREDENTIALS
def get_speech_service():
    credentials = GoogleCredentials.get_application_default().create_scoped(
        ['https://www.googleapis.com/auth/cloud-platform'])
    http = httplib2.Http()
    credentials.authorize(http)

    return discovery.build(
        'speech', 'v1', http=http, discoveryServiceUrl=DISCOVERY_URL)
# [END authenticating]

def process_audio(speech_file):
    """Transcribe the given audio file.
    Args:
        speech_file: the name of the audio file.
    """
    # [START construct_request]
    #posts = Environment(loader=FileSystemLoader('static/')) # Note that we use static_files folder defined in app.yaml
    #post = posts.get_template('audio.raw')
    #post = jinja_environment.get_template('audio.raw')
    #post = 'statics/audio.raw'
    #logging.info('This is an info message' + str(speech_file))
    #with open(speech_file, 'rb') as speech:
        # Base64 encode the binary audio file for inclusion in the JSON
        # request.
        #speech_content = base64.b64encode(speech.read())
    speech_content = base64.b64encode(speech_file)
    service = get_speech_service()
    service_request = service.speech().recognize(
        body={
            'initialRequest': {
                'encoding': 'LINEAR16',
                'sampleRate': 16000
            },
            'audioRequest': {
                'content': speech_content.decode('UTF-8')
                }
            })
    # [END construct_request]
    # [START send_request]
    response = service_request.execute()
    return response
    #print(json.dumps(response))
    # [END send_request]

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
