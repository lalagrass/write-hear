gRPC library for grpc-google-cloud-speech-v1

grpc-google-cloud-speech-v1 contains the IDL-generated library for the service:

cloud-speech - version v1

in the googleapis_ repository.


.. _`googleapis`: https://github.com/google/googleapis


